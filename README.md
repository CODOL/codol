# CODOL

## Premisas

**Tener en cuenta que el autor lleva años programando en C#**

>Premisas Abril 2022

- Dentro de 50, 100 o 300 años a ver quien es el "tio listo" que mantiene angularJS. Puede que haya una menor población, menos programadores, mas guerras por recursos. Lo último que necesitan es un lenguaje de programación complicado y que fue cambiando de sintaxis cada 3 meses.
- **Objetivo**: Generar transpiladores de otros lenguajes a Codol para que se puedan ir recogiendo todos los lenguajes abandonados.
- Evitar cansar las manos del programador poniendo teclas combinadas para escribir el lenguaje. Disminuir uso de:
  - Mayusculas
  - puntos y coma
  - Parentesis, interrogaciones, igual, menor/mayor que,...
- Tabulado (4 espacios) al igual que python para evitar el caos del codigo C#
- Maximo 150 caracteres por linea para evitar lineas "de aquí al rio" como en C#  
- Sólo existen para los tipos simples **string** o **number**
- Los **number** deben de poderse declarar y tener mayor precisión que en COBOL
- Se inspira de los lenguajes C#, COBOL, python, T-SQL

-----------------------------------------------------------------
>Imagen de una posible sintaxis inicial

![imagen](./imgs/Captura%20de%20pantalla_2022-04-27_21-48-15.png)

## CHANGELOG

### 2022 (desde Abril)

- Idea inicial
- Coloreado sintaxis mediante editor Nano en linux



