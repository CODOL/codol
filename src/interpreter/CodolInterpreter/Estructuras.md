# Estructura de cualquier programa CODOL

- program
    - use: importa librerias
    - lugar de inicio 
        - info programa (identificacion-division)
            - program-id
            - runtime : version de codol a ejecutar
        - main : solo inicia y llama al namespace.clase correspondiente
    - namespaces
        - classes
            - variables
            - sub-rutinas: metodos o funciones
                - variables
                - instrucciones
                    - instrucciones de accion: hacen una accion o cambian una variable
                    - instrucciones de llamada: llaman a otro metodo o funcion
                    - instrucciones de flujo

>**variables** puede ser de tipo: 
>- number
>- string
>- Objeto
>   - Datetime
>   - cualquier otro objeto
>- Enumerado 

______________________

