namespace CodolInterpreter
{
    internal class Interprete 
    {
        internal static string SPACES = "    ";
        internal static class Errors
        {
            public const string NOT_END_LABEL = "End label not found for: ";
            public const string NOT_END_LINE = "End dot not found for line: ";
        }

        internal List<string> TextoFichero { get; set; }
        internal ObjProgram Programa { get; private set; }

        public Interprete(List<string> textoFichero, string[] args)
        {
            this.TextoFichero = textoFichero;
            Programa = new ObjProgram(args);
        
        }

        public void LeerEstructura()
        {
            string epn = EspaciosPorNivel(1);
            System.Console.WriteLine("Se comienza a leer archivo");
            int eofLineas = this.TextoFichero.Count;
            for(int lc=0; lc<eofLineas; lc++)
            {
                string linea = this.TextoFichero[lc];
                System.Console.WriteLine($"{linea}");

                if (linea.ToLower().Contains("begin-identification"))
                {
                    #region begin-identification
                    var lineasBeginIdentificacion = ObtenerContenidoBegin(lc, "end-identification");  
                    List<string> lineasEj = LineasEjecucionCompleta(lineasBeginIdentificacion, 1);
                        
                    foreach(string linEj in lineasEj)
                    {
                        if(linEj.ToLower().Contains("program-id "))
                        {
                            Programa.ProgramInfo.ProgramId = linEj.Replace("program-id ", "");
                        }
                        if(linEj.ToLower().Contains("codol-runtime "))
                        {
                            Programa.ProgramInfo.CodolRuntime = linEj.Replace("codol-runtime ", "");
                        }
                    }

                    System.Console.WriteLine("Evaluado begin-identificacion...");
                    #endregion //begin-identification                    
                }
                else if (linea.ToLower().Contains("begin-main"))
                {
                    #region begin-main
                    var lineasBegin = ObtenerContenidoBegin(lc, "end-main");       
                    List<string> lineasEj = LineasEjecucionCompleta(lineasBegin, 1);

                    foreach(string linEj in lineasEj)
                    {
                        Programa.MainClassMethod = linEj.Replace("  ", " ").Replace("new ", "");
                    }

                    System.Console.WriteLine("Evaluado begin-main...");
                    #endregion //begin-main
                }
                else if (linea.ToLower().Contains("in-namespace"))
                {
                    ObjNamespace beginNamespace = new ObjNamespace();
                    beginNamespace.StructureName = linea.Replace(epn, "").Replace("in-namespace ", "").Replace(" begin", "");

                    var lineasBegin = ObtenerContenidoBegin(lc, "end-namespace");       
                    

                    Programa.Namespaces.Add(beginNamespace);

                    System.Console.WriteLine("Evaluado namespace...");

                    

                    /*
                    for (int linNamespace = 0; linNamespace < lineasEj.Count; linNamespace++)
                    {
                        string lnNamespace = lineasEj[linNamespace];    
                        if (lnNamespace.ToLower().Contains("begin-function "))
                        {
                            var lineasBeginfunc = ObtenerContenidoBegin(lc, "end-function");       
                            List<string> lineasEjfunc = LineasEjecucionCompleta(lineasBeginfunc, 3);

                            System.Console.WriteLine("func");
                        }
                        else if (lnNamespace.ToLower().Contains("begin-public-class "))
                        {
                            var lineasBeginclass = ObtenerContenidoBegin(lc, "end-class");       
                            List<string> lineasEjclass = LineasEjecucionCompleta(lineasBeginclass, 2);
                            ObjClass clase = new ObjClass();
                            clase.Parent = beginNamespace;
                            clase.StructureName = lnNamespace.Replace(epn, "").Replace("begin-public-class ", "");
                            beginNamespace.Classes.Add(clase);
                            System.Console.WriteLine("class");
                        }
                    }
                    */

                    
                }
            }
        }

        private List<string> ObtenerContenidoBegin(int lineaActual, string etiqueta)
        {
            int fin = ChequeoEncontrarEnd(lineaActual, etiqueta);
            return this.TextoFichero.GetRange(lineaActual+1, (fin-1)-lineaActual);
        }

        private List<string> LineasEjecucionCompleta(List<string> lineas, int nivel)
        {
            string epn = EspaciosPorNivel(1);
            string linea = string.Empty;
            List<string> lineasEjecucion = new List<string>();
            bool encontrado = false;
            for (int i = 0; i < lineas.Count; i++)
            {
                if(lineas[i].Replace(epn, "").StartsWith("//"))
                {
                    //ignorar de momento
                }
                else if(lineas[i].Replace(epn, "").StartsWith("begin-") || lineas[i].Replace(epn, "").StartsWith("end-"))
                {
                    encontrado = true;
                    linea += lineas[i];
                    linea = linea.Replace(epn, "");
                    lineasEjecucion.Add(linea);
                    linea = string.Empty; 
                }
                else if(lineas[i].TrimEnd().Length>1)
                {
                    if(lineas[i].Contains(". ") || lineas[i].TrimEnd()[lineas[i].Length-1] == '.')
                    {
                        encontrado = true;
                        linea += lineas[i];
                        linea = linea.Replace(epn, "");
                        lineasEjecucion.Add(linea);
                        linea = string.Empty;                    
                    }
                    else
                    {
                        linea += lineas[i];
                        encontrado = false;
                    }
                }               
            }

            if(encontrado == false && linea != string.Empty)
            {
                throw new Exception(Errors.NOT_END_LABEL + linea);
            }
            return lineasEjecucion;
        }
        private void ChequeoEspacios()
        {

        }
        private string EspaciosPorNivel(int nivel)
        {
            string espacios = "";
            for(int i=0; i<nivel; i++)
            {
                espacios += SPACES;
            }
            return espacios;
        }

        private int ChequeoEncontrarEnd(int startIn, string etiqueta)
        {
            int encontrado = -1;
            int eofLineas = this.TextoFichero.Count;
            for(int i=startIn; i<eofLineas; i++)
            {
                if(this.TextoFichero[i].ToLower().Contains(etiqueta))
                {
                    encontrado = i;
                }
            }
            
            if(encontrado == -1)
            {
                throw new Exception(Errors.NOT_END_LABEL + etiqueta);
            }
            return encontrado;
        }

        public void Ejecutar()
        {
            /*
                PENSANDO...
                    una vez se tenga la estructura del programa mirar como ejecutarlo
                    seguramente esto termine yendo en un metodo recursivo
            */
            string mainNamespace = Programa.MainClassMethod.Split('.')[0];
            string mainClass  = Programa.MainClassMethod.Split('.')[1];
            string mainSubrutine = Programa.MainClassMethod.Split('.')[2];
            
            var beginNamespace = Programa.Namespaces.Where(n => n.StructureName == mainNamespace).First();
            var beginClass = beginNamespace.Classes.Where(c => c.StructureName == mainClass).First();
            

            //ejecutar constructor
            foreach(var instruccion in beginClass.Constructor.Instrucciones)
            {
                if(instruccion.TipoInstruccion == InstructionType.ACTION)
                {

                }
                else if(instruccion.TipoInstruccion == InstructionType.CALL)
                {

                }
                else if(instruccion.TipoInstruccion == InstructionType.FLOW)
                {

                }
            }
            //ejecutar subrutina
            var mainSub = beginClass.SubRutinas.Where(sub => sub.StructureName == mainSubrutine).First();

            foreach(var instruccion in mainSub.Instrucciones)
            {
                if(instruccion.TipoInstruccion == InstructionType.ACTION)
                {

                }
                else if(instruccion.TipoInstruccion == InstructionType.CALL)
                {

                }
                else if(instruccion.TipoInstruccion == InstructionType.FLOW)
                {
                    
                }
            }
        }   
    }
}