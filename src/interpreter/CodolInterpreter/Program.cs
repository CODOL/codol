﻿using System;

namespace CodolInterpreter
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //para arrancarlo poner $> dotnet run -- archivo.codol
            //dotnet run -- /media/DATOS/PROYECTOS/LenguajeMantenible/codol/src/dos.codol
            if (args.Count() == 0)
            {
                System.Console.WriteLine("Añada el archivo de entrada");
            }
            else if (System.IO.File.Exists(args[0]) == false)
            {
                System.Console.WriteLine("Ruta de archivo no valida");
            }
            else
            {
                string fileNameArg = args[0];
                string filenName = fileNameArg.Split('.')[0];
                string extension = fileNameArg.Split('.')[1].ToLower();
                if (extension == "codol")
                {
                    string actualDir = System.IO.Directory.GetCurrentDirectory();
                    List<string> textoFichero = new List<string>();

                    foreach (string line in System.IO.File.ReadLines(fileNameArg))
                    {
                        textoFichero.Add(line.Replace("\t", Interprete.SPACES));
                    }

                    if (textoFichero.Count() == 0)
                    {
                        System.Console.WriteLine("Error, no hay contenido o no se puede leer");
                    }
                    else
                    {
                        LlamarInterprete(textoFichero, args);
                    }
                }
                else
                {
                    //archivo de proyecto
                    System.Console.WriteLine("Error archivo de proyecto, aun no soportado");
                }
            }
        }//end-main

        private static void LlamarInterprete(List<string> textoFichero, string[] args)
        {
            Interprete interprete = new Interprete(textoFichero, args);
            interprete.LeerEstructura();
        }

    }//end-class
}