namespace CodolInterpreter
{
    internal class ObjClass : CodolBase
    {
        public List<CodolVariablesBase> ClassVariables { get; set; }

        public ObjSubrutina Constructor { get; set; }

        public List<ObjSubrutina> SubRutinas { get; set; }

        public ObjClass()
        {
            ClassVariables = new List<CodolVariablesBase>();
            SubRutinas = new List<ObjSubrutina>();
            Constructor = new ObjSubrutina();          
        }
    }
}