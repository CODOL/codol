
namespace CodolInterpreter
{
    internal class CodolBase
    {
        /// <summary>
        /// Indica el nombre del elemento tratado
        /// </summary>
        /// <value></value>
        public string StructureName { get; set; }
        public CodolBase Parent { get; set; }   

        public CodolBase()
        {
            StructureName = string.Empty;
            Parent = null;
        }
    }
}