namespace CodolInterpreter
{
    internal class ObjNamespace :CodolBase
    {
        public List<ObjClass> Classes { get; set; }

        public ObjNamespace()
        {
            Classes = new List<ObjClass>();
        }        
    }
    
}