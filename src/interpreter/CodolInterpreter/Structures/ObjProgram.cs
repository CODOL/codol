
namespace CodolInterpreter
{
    internal class ObjProgram
    {
        public List<ObjUse> Uses { get; set; }
        public List<ObjNamespace> Namespaces { get; set; }    
        public string MainClassMethod { get; set; }
        public ObjProgramInfo ProgramInfo { get; set; }
        public string[] ProgramArguments { get; set; }
        
        public ObjProgram(string[] args)
        {
            Uses = new List<ObjUse>();
            Namespaces = new List<ObjNamespace>();
            MainClassMethod = string.Empty;
            ProgramInfo = new ObjProgramInfo();
            ProgramArguments = args;
        }
    }
}