
namespace CodolInterpreter
{
    internal class ObjProgramInfo : CodolBase
    {
        public string ProgramId { get; set; }
        public string CodolRuntime { get; set; }

        public ObjProgramInfo()
        {
            ProgramId = string.Empty;
            CodolRuntime = string.Empty;
        }

    }
}