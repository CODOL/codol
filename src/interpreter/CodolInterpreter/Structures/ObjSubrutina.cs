namespace CodolInterpreter
{
    internal enum SubrutinaType
    {
        /// <summary>
        /// Cuando no tiene elementos a retornar
        /// </summary>
        METHOD,
        /// <summary>
        /// Cuando retorna uno o mas elementos
        /// </summary>
        FUNCTION
    }
    internal class ObjSubrutina : CodolBase
    {
        public SubrutinaType TipoSubrutina { get; set; }
        public List<CodolVariablesBase> Variables { get; set; }

        public List<ObjInstruction> Instrucciones { get; set; }

        public ObjSubrutina( )
        {
            Variables = new List<CodolVariablesBase>();
            TipoSubrutina = SubrutinaType.METHOD;
            Instrucciones = new List<ObjInstruction>();
        }
    }
}