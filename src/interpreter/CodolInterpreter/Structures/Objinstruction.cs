
namespace CodolInterpreter
{
    internal enum InstructionType
    {
        /// <summary>
        /// Acciones de cambio de valor o pintar por pantalla
        /// </summary>
        ACTION,
        /// <summary>
        /// Control de flujo
        /// </summary>
        FLOW,
        /// <summary>
        /// Llamada a otra subrutina
        /// </summary>
        CALL
    }
    internal class ObjInstruction : CodolBase
    {
        public InstructionType TipoInstruccion { get; set; }

        public ObjInstruction()
        {
            TipoInstruccion = InstructionType.ACTION;
        }
    }
}