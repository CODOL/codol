﻿using System;
using System.Text;

namespace CodolNet
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //para arrancarlo poner $> dotnet run -- archivo.codol
            if(args.Count() ==0)
            {
                System.Console.WriteLine("Añada el archivo de entrada");
            }
            else
            {
                //comenzamos la transpilacion
                string fileNameArg = args[0];
                string filenName = fileNameArg.Split('.')[0];
                string extension = fileNameArg.Split('.')[1];
                if(extension == "codol")
                {
                    string actualDir = System.IO.Directory.GetCurrentDirectory();
                    string rutaArchivo = Path.Combine(actualDir, fileNameArg);
                    List<string> textoFichero = new List<string>();
                    foreach (string line in System.IO.File.ReadLines(rutaArchivo))
                    {  
                          textoFichero.Add(line);
                    }  
                    
                    if(textoFichero.Count() == 0)
                    {
                        System.Console.WriteLine("Error, no hay contenido o no se puede leer");
                    }
                    else
                    {
                        string archivoSalida = Path.Combine(actualDir, filenName + ".cs");
                        Transpilar(archivoSalida, textoFichero);
                    }
                }
                else
                {
                    //archivo de proyecto
                    System.Console.WriteLine("Error archivo de proyecto, aun no soportado");
                }
            }
        }//end main

        private static void Transpilar(string archivoSalida, List<string> textoCodol)
        {
            StringBuilder csharp = new StringBuilder();
            csharp.AppendLine("using System;");
            
            foreach(string lineaActual in textoCodol)
            {              
                bool addOpenBracket = false;
                bool addCloseBracket = false;
                StringBuilder linea = new StringBuilder(lineaActual);  
                if(linea.ToString().ToLower().StartsWith("in-namespace "))
                {
                    linea = QuitaEspacios(linea);
                    linea.Replace("in-namespace", "namespace");
                    linea = SustituirFinalLinea(linea);             
                }
                else if(linea.ToString().ToLower().StartsWith("begin-public-class "))
                {
                    linea = QuitaEspacios(linea);
                    linea.Replace("begin-public-class", "public class");
                    linea.Replace(" program", " Program");
                    addOpenBracket = true;
                }
                else if(linea.ToString().ToLower().StartsWith("begin-internal-class "))
                {
                    linea = QuitaEspacios(linea);
                    linea.Replace("begin-internal-class", "internal class");
                    linea.Replace(" program", " Program");
                    addOpenBracket = true;
                }
                else if(QuitaEspacios(linea).ToString().TrimStart().ToLower().StartsWith("begin-public-method "))
                {
                    linea = QuitaEspacios(linea);
                    
                    if(linea.ToString().Contains(" main"))
                    {
                        linea.Replace("begin-public-method", "public static void");
                    }
                    else
                    {
                        linea.Replace("begin-public-method", "public void");
                    }
                    linea.Replace(" main", " Main");
                    addOpenBracket = true;
                    if(linea.ToString().Contains(" with "))
                    {
                        linea.Replace(" with", "(");
                    }
                    
                    linea = Variables(linea);
                    linea.Append(")");
                }
                else if(QuitaEspacios(linea).ToString().TrimStart().ToLower().StartsWith("end-method"))
                {
                    linea.Replace("end-method", "}//end-method");
                }
                else if(QuitaEspacios(linea).ToString().TrimStart().ToLower().StartsWith("end-class"))
                {
                    linea.Replace("end-class", "}//end-class");
                }
                else if(QuitaEspacios(linea).ToString().TrimStart().ToLower().StartsWith("display-line "))
                {
                    linea.Replace("display-line ", "System.Console.WriteLine(");
                    linea.Replace("\'", "\"");
                    linea = SustituirFinalLinea(linea);
                    linea.Append(";");
                }
                else if(QuitaEspacios(linea).ToString().TrimStart().ToLower().StartsWith("display "))
                {
                    linea.Replace("display ", "System.Console.Write(");
                    linea.Replace("\'", "\"");
                    linea = SustituirFinalLinea(linea);
                    linea.Replace(";", ");");
                }

                csharp.AppendLine(linea.ToString());
                if(addOpenBracket)
                {
                    addOpenBracket = false;
                    csharp.AppendLine("{");
                }
                else if(addCloseBracket)
                {
                    addCloseBracket = false;
                    csharp.AppendLine("}");
                }
            }


            using(var escritor = new StreamWriter(archivoSalida, false, new System.Text.UTF8Encoding(true)))
            {
                escritor.WriteLine(csharp);
            }
            string projectFile = "<Project Sdk=\"Microsoft.NET.Sdk\">" +
            "  <PropertyGroup> " +
            "    <OutputType>Exe</OutputType> " +
            "    <TargetFramework>net6.0</TargetFramework> " +
            "    <ImplicitUsings>enable</ImplicitUsings> " +
            "    <Nullable>enable</Nullable> " +
            "  </PropertyGroup> " +
            "</Project>"; 
            string archivoProyecto = archivoSalida.Replace(".cs", ".csproj");
            using(var escritor = new StreamWriter(archivoProyecto, false, new System.Text.UTF8Encoding(true)))
            {
                escritor.WriteLine(projectFile);
            }
        }

        private static StringBuilder SustituirFinalLinea(StringBuilder linea)
        {
            int dot = linea.ToString().LastIndexOf(".");
            linea[dot] = ';';  
            return linea;
        }
        private static StringBuilder Variables(StringBuilder linea)
        {
            if(linea.ToString().TrimStart().ToLower().Contains("string-array"))
            {
                linea.Replace("string-array", "string[]");
            }
            /*if(linea.ToString().TrimStart().ToLower().Contains("number-array"))
            {
                linea.Replace("number-array", "string[]");
            }*/
            return linea;
        }
        private static StringBuilder QuitaEspacios(StringBuilder linea)
        {
            StringBuilder linea2 = linea;
            while(linea2.ToString().Contains("  "))
            {
                linea2 = linea2.Replace("  ", " ");
            }
            return linea2;
        }
    }//end class
}
