"""Codol transpiler"""
import sys

def read_input_file(path):
    """read input file"""
    data = []

    with open(path, "r", encoding="utf-8") as file:
        for line in file:
            data.append(line)
    file.close()
    return data

def write_output_file(path, outlines):
    """write output python file"""
    new_file = path + ".py"
    with open(new_file, 'w', encoding="utf-8") as file:
        file.writelines(outlines)

    file.close()

def evaluate_print(line):
    """function print"""
    if "console.write-line " in line:
        line = line.replace("console.write-line ", "print(")
        line = line.replace("'", "\"")
        line = line.replace(" with ", ",")
        line = line.replace("...", "#dots#")
        line = line.replace(" ..", " {")
        line = line.replace("..", "} ")
        line = line.replace("#dots#", "...")
        line = line.replace("\n", ")\n")

    return line

def determine_data_type_python(nombre):
    """determine the python data type from codol data type"""
    python_type = "str"
    array_type = ""
    resultado = ""

    if "-array" in nombre:
        array_type = "List"
    elif "-tuple" in nombre:
        array_type = "Tuple"
    elif "-dict" in nombre:
        array_type = "Dict"

    if ("string" in nombre):
        python_type = "str"
    elif ("number" in nombre):
        python_type = "int"
    elif ("integer" in nombre):
        python_type = "int"
    elif ("float" in nombre):
        python_type = "float"

    if array_type != "":
        resultado = f"{array_type}[{python_type}]"
    else:
        resultado = python_type
    return resultado

def evaluate_main(line):
    """evaluate begin-main"""
    if "begin-main " in line:
        line = line.replace("  ", " ")
        line = line.replace("\n", "")
        line = line.replace("begin-main ", "def main(")
        if "with " in line:
            argumentos = line.split("with ")[1]
            variables = argumentos.split(",")

            for variable in variables:
                variable = variable.split(" then")[0]
                tipo = variable.lstrip().split(" ")[0]
                nombre_variable = variable.lstrip().split(" ")[1].split(" then")[0]
                tipo_python = determine_data_type_python(tipo)
                line = line.replace(variable, f"{nombre_variable}:{tipo_python}")
        line = line.replace("with ", "")
        line = line.replace(" then", "):")
        line += "\n"
    elif "end-main" in line:
        line = line.replace("end-main", "#end-main")
    return line

def main(main_args):
    """main function"""
    if len(main_args) == 0 :
        print("error, faltan argumentos")
    else:
        outlines = []
        lines = read_input_file(main_args[0])
        for line in lines:
            output = evaluate_print(line)
            output = evaluate_main(output)
            outlines.append(output)

        print(outlines)
        outlines = ["from typing import Dict, Tuple, Sequence, List\n", *outlines]
        #if "def main" in outlines:
        #     outlines = [*outlines, "main()"]

        write_output_file(main_args[0], outlines)


if __name__ == "__main__":
    args = sys.argv[1:]
    main(args)

#end
